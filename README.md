# Android for the Raspberry Pi 4

This is an unfinished project about porting Android to the Raspberry Pi. Since this is not finished, we can't provide compiled images, but we'll upload one as soon as we get one working.

## Downloading AOSP source with patches (local_manifests)

 Refer to http://source.android.com/source/downloading.html
 
```
repo init                  \
     -u https://android.googlesource.com/platform/manifest \
     -b android-10.0.0_r25  \
     --current-branch      \
     --no-tags             \
     --no-clone-bundle     \
     --depth 1
git clone https://gitlab.com/rpi-droid/rpi4/local_manifests.git .repo/local_manifests -b android-10
repo sync --current-branch --no-tags --no-clone-bundle
```

## How to compile

We only provide a tutorial for Debian GNU/Linux based operating systems, which run on an i386 or amd64 architecture. We will provide tutorials for other GNU/Linux based operating systems after we got a working build.

**Warning:** This is currently not working on WSL.

To compile android with this project, you need to run this in bash:

```
sudo apt install gcc-aarch64-linux-gnu libssl-dev python-mako git bison flex curl
mkdir ~/bin
curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo
mkdir ~/android
cd ~/android
repo init -u https://android.googlesource.com/platform/manifest -b android-10.0.0_r25
git clone https://gitlab.com/rpi-droid/rpi4/local_manifests.git .repo/local_manifests -b android-10
repo sync -j$(nproc)
source build/envsetup.sh
lunch rpi4-eng
cd kernel/rpi
ARCH=arm64 scripts/kconfig/merge_config.sh arch/arm64/configs/bcm2711_defconfig kernel/configs/android-base.config kernel/configs/android-recommended.config
ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- make Image dtbs -j$(nproc)
cd ../..
make ramdisk systemimage vendorimage -j$(nproc)
```

**Warning:** This needs much CPU power, and you probably can't use the computer building this while these steps are running. You need at least 8GB of RAM, more is recommended (you can also use an swapfile or swap partition).

## How to install

### Prepare sd card

Partitions of the card should be set-up like followings.

- p1 256MB for BOOT : Do fdisk : W95 FAT32(LBA) & Bootable, mkfs.vfat
- p2 640MB for /system : Do fdisk, new primary partition
- p3 128MB for /vendor : Do fdisk, new primary partition
- p4 remainings for /data : Do fdisk, mkfs.ext4
- Set volume label for /data partition as userdata 

use -L option of mkfs.ext4, e2label command, or -n option of mkfs.vfat

You can also use GParted.

### Write system & vendor partition

Run these commands:

```
cd out/target/product/rpi4
sudo dd if=system.img of=/dev/<p2> bs=1M
sudo dd if=vendor.img of=/dev/<p3> bs=1M
```

### Copy kernel & ramdisk to BOOT partition
Copy

device/brcm/rpi4/boot/* to p1:/

kernel/rpi/arch/arm64/boot/Image to p1:/

kernel/rpi/arch/arm64/boot/dts/bcm2711-rpi-4-b.dtb to p1:/

kernel/rpi/arch/arm64/boot/dts/overlays/vc4-kms-v3d.dtbo to p1:/overlays/vc4-kms-v3d.dtbo

out/target/product/rpi4/ramdisk.img to p1:/
