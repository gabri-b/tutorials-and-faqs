# CHEAT SHEET 

## Download `repo` tool

Fill in "Your Name" and "you@example.com" with proper values:

```bash
mkdir  ~/bin
echo   'export PATH=~/bin:$PATH' >> ~/.bashrc
source ~/.bashrc

curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo

git config --global user.name "Your Name"
git config --global user.email "you@example.com"
```

## AOSP source
 
This example fetches the android version `android-10.0.0_r25` from google, and
adds Raspberry Pi specific local manifest:

```bash
repo init                  \
    -u https://android.googlesource.com/platform/manifest \
    -b android-10.0.0_r25  \
    --current-branch      \
    --no-tags             \
    --no-clone-bundle     \
    --depth 1             \
    -j$(nproc)
git clone \
  https://gitlab.com/rpi-droid/rpi4/local_manifests.git \
  .repo/local_manifests \
  -b android-10
repo sync --current-branch --no-tags --no-clone-bundle
```

## Downloading Lineage source

This example fetches the android version `15.1` from github:
// TODO Raspberry Pi manifest?

```bash
repo init                  \
    -u git://github.com/LineageOS/android.git \
    -b lineage-15.1       \
    --current-branch      \
    --no-tags             \
    --no-clone-bundle     \
    --depth 1             \
    -j$(nproc)
repo sync --current-branch --no-tags --no-clone-bundle
```

## Building the kernel

```bash
cd kernel/rpi
ARCH=arm64 scripts/kconfig/merge_config.sh      \
           arch/arm64/configs/bcm2711_defconfig \
           kernel/configs/android-base.config   \ 
           kernel/configs/android-recommended.config
ARCH=arm64 CROSS_COMPILE=aaarch64-linux-gnu- \
           make Image dtbs -j$(nproc)
```

## Building the Android

```bash
# make clean                                    # if build failed, start clean.
source build/envsetup.sh
lunch                                           # you'll have to choose RPi then.
make -j$(nproc) ramdisk systemimage vendorimage 
```
