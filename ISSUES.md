# Report a bug

This is a guide about reporting a bug in this project.

1. Look if the bug is already in the [FAQ](FAQ.md).
2. If not, look at the [issues](https://gitlab.com/rpi-droid/rpi4/bugs/issues).
3. If the bug is not there, [open a new issue](https://gitlab.com/rpi-droid/rpi4/bugs/issues/new). You can also join our [discord server](https://discord.gg/kf4ZPqP) for additional help.