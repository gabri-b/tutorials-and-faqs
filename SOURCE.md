# The Source Code

An Android OS project is not a single project, but a conglomerate of tens and
hundreds of other projects. The tool to download and manage all these projects
is called `repo`. It simply a wrapper around `git`.

Refer to http://source.android.com/source/downloading.html for more
informataion.

Please note, that the android source code is _huge_ it could take hours to
download depending on your connection speed.

## Downloading the repo tool

First, you download a little script called `repo`. It will take care of
downloading the actual repo tool and the android source code for you:

```bash
mkdir ~/bin
export PATH=~/bin:$PATH

curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo

git config --global user.name "Your Name"
git config --global user.email "you@example.com"
```

You can put `export PATH=....` in .bashrc to always have `repo` tool available.
Now you're ready to download the android source. 

## Optimizing checkout speed

As of writing this, the `repo` tool has two commands which can be optimized
using these flags.

#### Optimizing repo init

Always call repo init like this: 

```bash
repo init --current-branch --no-tags --no-clone-bundle --depth 1 -j$(nproc) \
     -u THE_URL
```

Refer to `repo`'s help for more information: `repo init --help`

#### Optimizing repo sync

Always call repo sync like this: 

```bash
repo sync --current-branch --no-tags --no-clone-bundle -j$(nproc)
```

Refer to `repo`'s help for more information: `repo sync --help`

#### Optimize speed

Because repo uses git, it downloads compressed files. It can take some time to
decompress them, but repo can run it on all your cpu cores to be faster! Just
add the `-j(how many cores should id use)` option.
Run it like this to use all core:

```
repo sync -j$(nproc) # nproc determines number of cpu cores automatically for you.
```

## Source structure

Android source code looks like this:

```bash
tree -L 1
.
├── Android.bp -> build/soong/root.bp
├── Makefile
├── android
├── art
├── autoload
├── bionic
.
. # 30 lines removed for brevity.
.
└── .repo
    ├── local_manifests
    ├── manifest.xml -> manifests/default.xml
    ├── manifests
    ├── manifests.git
    ├── project-objects
    ├── project.list
    ├── projects
    └── repo
```

The top directories are the projects that need to be built individually (or
depending on other directories) for Android to compile. How does `repo` tool
know what to download? It does so by looking at the _manifest_ files. Manifest
is an xml file specifying the names and address of each project on internet
that should be downloaded.

The `.repo` directory is a special directory: it's like `.git`, let's go
through it:

- repo: This is the actual repo application, called by the `repo` tool.
- projects-XXX: If you delete all the top directories (except for .repo of
  course) the repo tool can re-create them by reading these directories without
  downloading them again. These are the actual `.git` files.
- manifests, manifests.git, manifest.xml these are the manifest files
  _managed by the repo tool_ that you do not edit directly.
- local_manifests: this is where you modify the project. If you want some
  specific directory or project removed/added to the android source tree, you
  name it in the local_manifests.

## Downloading Android source

Now you are ready to use the `repo` tool to download the source code. `repo`
requires a manifest to know what to download, the branch (read it: the android
version, aka kitkat, oreo, 10, ...) to download and some optional flags.

Repo has three stages:

1. Initializing the source directory and specifying the manifest/branch.
2. Specifying custom modifications to source tree in local_manifests.
3. Calling `sync` to get the actual source code.

### Downloading AOSP source
 
This example fetches the android version `android-10.0.0_r25` from google, and
adds Raspberry Pi specific local manifest:

```bash
repo init                  \
    -u https://android.googlesource.com/platform/manifest \
    -b android-10.0.0_r25  \
    --current-branch      \
    --no-tags             \
    --no-clone-bundle     \
    --depth 1             \
    -j$(nproc)
git clone \
  https://gitlab.com/rpi-droid/rpi4/local_manifests.git \
  .repo/local_manifests \
  -b android-10
repo sync --current-branch --no-tags --no-clone-bundle
```

### Downloading Lineage source
 
This example fetches the android version `15.1` from github:

```bash
repo init                  \
    -u git://github.com/LineageOS/android.git \
    -b lineage-15.1       \
    --current-branch      \
    --no-tags             \
    --no-clone-bundle     \
    --depth 1             \
    -j$(nproc)
repo sync --current-branch --no-tags --no-clone-bundle
```

## What do the files do?

There are much files, and you maybe want to know what they do. We'll try to explain them,
but we won't explain every single file, but the important ones.

### Android.bp

This file is a file for soong and some kind of Makefile. Soong is an alternative to make
and used for android. Soong is written in go Soong files use JSON formatting. And the files
aren't very complicated. They just define what to build, with wich flags (for gcc/clang)
and how the result is called. Soong is written in Go.

### Makefile

The first version of android were build with make, and this file stayed, since you can't easily
use soong for compiling, so the Makefile just runs soong. For compatiblity reasons, make also stays,
because some parts of android still have a legacy Android.mk instad of an Android.bp. 

### build/envsetup.sh

This file contains some function for bash. You can run it with `source build/envsetup.sh`, not
directly as script. These functions are defined by this file (there are more, just the important
ones are listed her):

- _hmm_: More information about the android build system.
- _lunch_: Tool to select which device should be build for.
- _m_: Runs make, but you can be in every subdirectory of the android code, it always runs from top.
